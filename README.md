# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* LSFR Version 2.0 (Python 3 version) modified to work with Forced Oscillations Experiment
* LSFR Version 3.0

### How do I get set up? ###

* Run with python3
* Requires numpy, scipy packages
* Requires tkinter package

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* edward.birdsall@postgrad.manchester
* (Maintainer of this repository.)